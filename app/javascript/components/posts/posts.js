import React from 'react';
import { Post } from './post';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export class Posts extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      posts: []
    }
  }

  componentWillReceiveProps(nextProps){
    this.setState({
      posts: nextProps.posts
    })
  }

  componentDidMount(){
    this.subscribe();
  }

  publications(){
    if(this.state.posts){

      return this.state.posts.map(post => {
        return <Post  key={post.id} post={post}></Post>;
      });
    }

    return "";
  }

  subscribe(){
    App.post = App.cable.subscriptions.create("PostChannel",{
      connected: ()=>{
        console.log("Suscrito a la websocket");
      },
      disconnected: ()=>{

      },
      received: (data)=>{
        let post = JSON.parse(data.data);

        this.setState({
          posts: [post].concat(this.state.posts);
        })

      }
    });

  }

  render(){
    return(
      <MuiThemeProvider>
        <div>
          {this.publications()}
        </div>
      </MuiThemeProvider>
    );
  }
}
